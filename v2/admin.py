from django.contrib import admin
from .models import Profile
from .views import web_page

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['name']
    actions = ['generate']

    def generate(self, request, queryset):
        if len(queryset) == 1:
            return web_page(request, queryset[0])
        else:
            self.message_user(request, "Please, select exactly one profile to generate new spendings")
    generate.short_description = "Generate new spendings"
