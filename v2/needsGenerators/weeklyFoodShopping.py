import datetime as d
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=3, hour=19),
          periodDay = 7,
          triggerTimeRandomRange = 0,
          triggerCancellationProbability=5,
  )

  # Strength and StrengthRandomRange
  stre = 0
  streRand = 0
  for p in pl:
    plusStre = 0
    plusStreRand = 0
    if p.adult:
      if p.gender == 'M': (plusStre, plusStreRand) = (15,5)
      if p.gender == 'F': (plusStre, plusStreRand) = (13,4)
    else:
      if p.school == 'Elementary': (plusStre, plusStreRand) = (17,4)
      if p.school == 'Middle': (plusStre, plusStreRand) = (15,4)
      if p.school == 'High': (plusStre, plusStreRand) = (16,4)
      if p.school == 'College': (plusStre, plusStreRand) = (17,4)  
    stre = stre + plusStre
    streRand = streRand + plusStreRand

  # Needs
  n = Need(
    name = "Weekly food shopping",
    schedule = s,
    subcategory = "GroceryStore",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = True,
  )
  return [n]
