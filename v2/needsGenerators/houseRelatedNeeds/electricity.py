import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=13, hour=20),
          periodDay = 60,
          triggerTimeRandomRange = 4,
          triggerCancellationProbability=0,
  )

  # Strength and StrengthRandomRange
  stre = 0
  streRand = 0

  # Heating if it is done by electricity
  if profile.heating == 'ELEC':
    houseSize = 0
    for p in pl:
      if p.adult: houseSize = houseSize +1
    else: houseSize = houseSize + 1/2
    stre = stre + houseSize * 20
    streRand = streRand + houseSize * 20

  # Normal electricity consumption
  for p in pl:
    stre = stre + 10
    streRand = streRand + 2

  # Needs
  n = Need(
    name = "Electricity",
    schedule = s,
    subcategory = "EnergySupplier",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = False,
  )
  return [n]
