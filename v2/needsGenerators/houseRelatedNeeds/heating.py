import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # FireWood heating
  if profile.heating == 'WOOD':
    #Chimney-sweeping
    s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=1, hour=14),
          periodDay = 0,
          triggerTimeRandomRange = 364,
          triggerCancellationProbability=0,
    )
    stre = 60
    streRand = 20
    chimneySweeping = Need(
      name = "Chimney-sweeping",
      schedule = s,
      subcategory = "EnergySupplier",
      strength = stre,
      strengthRandomRange = streRand,
      isBuyAction = True,
    )

    #Buying wood
    s = Schedule(
      firstDay = d.datetime(year=2017, month=1, day=1, hour=14),
      periodDay = 0,
      triggerTimeRandomRange = 364,
      triggerCancellationProbability=0,
    )
    houseSize = 0
    for p in pl:
      if p.adult: houseSize = houseSize + 1
      else: houseSize = houseSize + 1/2 
    stre = houseSize * 70
    streRand = houseSize * 10
    buyingWood = Need(
      name = "Wood",
      schedule = s,
      subcategory = "EnergySupplier",
      strength = stre,
      strengthRandomRange = streRand,
      isBuyAction = True,
    )

    return [chimneySweeping, buyingWood]
  else:
    return []

  # The ELEC heating is dealt with in the Electricity need
