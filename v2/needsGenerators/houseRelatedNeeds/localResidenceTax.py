import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=11, day=1, hour=20),
          periodDay = 0,
          triggerTimeRandomRange = 2,
          triggerCancellationProbability=0,
  )

  # Strength and StrengthRandomRange
  stre = 650
  streRand = 100

  # Needs
  n = Need(
    name = "Local Residence Tax",
    schedule = s,
    subcategory = "GovernmentOrganization",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = False,
  )
  return [n]
