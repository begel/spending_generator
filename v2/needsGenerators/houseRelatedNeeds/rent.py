import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=3, hour=20),
          periodDay = 30,
          triggerTimeRandomRange = 0,
          triggerCancellationProbability=0,
  )

  # Strength and StrengthRandomRange
  # Base rent for one person
  stre = 350 + r.randrange(101)
  streRand = 0
  firstPerson = True #Ignore the first person when adding strength to the need, because he is already accounted for by the base rent
  for p in pl:
    if firstPerson:
      firstPerson = False
    else:
      plusStre = 0
      plusStreRand = 0
      if p.adult: (plusStre, plusStreRand) = (100 + r.randrange(61),0)
      else:
        (plusStre, plusStreRand) = (50+r.randrange(21),0)
      stre = stre + plusStre
      streRand = streRand + plusStreRand

  # Needs
  n = Need(
    name = "Rent",
    schedule = s,
    subcategory = "RealEstateAgent",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = False,
  )
  return [n]
