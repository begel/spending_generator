import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=13, hour=20),
          periodDay = 170,
          triggerTimeRandomRange = 5,
          triggerCancellationProbability=0,
  )

  # Strength and StrengthRandomRange
  stre = 0
  streRand = 0
  for p in pl:
    plusStre = 0
    plusStreRand = 0
    if p.adult: (plusStre, plusStreRand) = (45 + r.randrange(10),5)
    else:
      (plusStre, plusStreRand) = (45+r.randrange(10),5)
    stre = stre + plusStre
    streRand = streRand + plusStreRand

  # Needs
  n = Need(
    name = "Water",
    schedule = s,
    subcategory = "WaterSupplier",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = False,
  )
  return [n]
