import datetime as d
import random as r
from v2.NeedAndSchedule import *

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Schedule
  s = Schedule(
          firstDay = d.datetime(year=2017, month=9, day=3, hour=20),
          periodDay = 0,
          triggerTimeRandomRange = 0,
          triggerCancellationProbability=0,
  )

  # Strength and StrengthRandomRange
  stre = 0
  streRand = 0
  for p in pl:
    plusStre = 0
    plusStreRand = 0
    if p.school == 'High':
        (plusStre, plusStreRand) = (500, 500) 
    stre = stre + plusStre
    streRand = streRand + plusStreRand

  # Needs
  if (stre != 0) or (streRand != 0):
    n = Need(
      name = "High School application",
      schedule = s,
      subcategory = "HighSchool",
      strength = stre,
      strengthRandomRange = streRand,
      isBuyAction = False,
    )
    return [n]
  else: 
    return []
