import datetime as d
import math
import random as r
from v2.NeedAndSchedule import *

# Add more hollidays?

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  # Christmas Hollidays

  goingOnHolliday = 0
  # Low
  if profile.travel_friendly == '1': goingOnHolliday = 10
  # Medium
  if profile.travel_friendly == '2': goingOnHolliday = 50
  # Hard
  if profile.travel_friendly == '3': goingOnHolliday = 90
  # Going on holliday
  s = Schedule(
    firstDay = d.datetime(year=2017, month=7, day=3, hour=8),
    periodDay = 0,
    triggerTimeRandomRange = 30,
    triggerCancellationProbability=goingOnHolliday,
  )
  stre = 0
  streRand = 0
  lengthOfTravel = r.randrange(3000)
  for p in pl:
    stre = stre + lengthOfTravel * 0.1
    streRand = streRand + lengthOfTravel * 0.05

  summerHollidaysNedd = Need(
    name = "Summer hollidays",
    schedule = s,
    subcategory = "TransportOrganization",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = True,
  )

  return [summerHollidaysNedd]
