import datetime as d
import math
import random as r
from v2.NeedAndSchedule import *

# Distinction based on whether the house is in a city or in the countryside ?

def generateNeeds(profile):
  # pl = list of the persons in the profile
  pl = profile.people.all()

  nbAdults = 0
  for p in pl: 
    if p.adult: nbAdults = nbAdults + 1
  nbCars = math.ceil(nbAdults/2)

  travelDistancePerMonth = 0
  for p in pl:
    travelDistancePerMonth = travelDistancePerMonth + 1500
    # Low
    if p.work_distance == '1': travelDistancePerMonth = travelDistancePerMonth + r.randrange(500)
    # Medium
    if p.work_distance == '2': travelDistancePerMonth = travelDistancePerMonth + 1000 + r.randrange(1000)
    # High
    if p.work_distance == '3': travelDistancePerMonth = travelDistancePerMonth + 2500 + r.randrange(5000)

  # Fuel
  s = Schedule(
          firstDay = d.datetime(year=2017, month=1, day=1, hour=18),
          periodDay = 14,
          triggerTimeRandomRange = 7,
          triggerCancellationProbability=0,
  )
  stre = math.ceil(travelDistancePerMonth * 0.005)
  streRand = math.ceil(travelDistancePerMonth * 0.000001)
  fuelNeed = Need(
    name = "Fuel for cars",
    schedule = s,
    subcategory = "GasStation",
    strength = stre,
    strengthRandomRange = streRand,
    isBuyAction = True,
  )

  return [fuelNeed]
