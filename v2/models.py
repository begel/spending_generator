from __future__ import unicode_literals
from django.db import models
from common.models import Person

zones = (
    ('RUR', 'Rural'),
    ('URB', 'Urban')
)
heating = (
    ('ELEC', 'Electric'),
    ('WOOD', 'Wood-fired'),
)
scale = (
    ('1', 'Low'),
    ('2', 'Medium'),
    ('3', 'High'),
)

class Profile(models.Model):
    name = models.CharField(max_length=50, default='Default')
    zone = models.CharField(max_length=3, choices=zones, default='RUR')
    heating = models.CharField(max_length=4, choices=heating, default='ELEC')
    travel_friendly = models.CharField(max_length=1, choices=scale, default='1')
    tech_friendly = models.CharField(max_length=1, choices=scale, default='1')
    clothes_friendly = models.CharField(max_length=1, choices=scale, default='1')
    leisure_friendly = models.CharField(max_length=1, choices=scale, default='1')
    people = models.ManyToManyField(Person)
    
    def __str__(self):
        return self.name
