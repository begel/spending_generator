class Schedule:
	# firstDay: datetime, first day at which the need should trigger
	# periodDay: int, periodicity at which the need should trigger, in days. 0 for no period at all
	# triggerTimeRandomRange: int, up to how many days will randomly be added to any trigger of the need
	# triggerCancellationProbability: int, percentage for a given trigger of this need NOT to actually go through
	def __init__(self, firstDay, periodDay, triggerTimeRandomRange, triggerCancellationProbability):
		self.firstDay = firstDay
		self.periodDay = periodDay
		self.triggerTimeRandomRange = triggerTimeRandomRange
		self.triggerCancellationProbability = triggerCancellationProbability
class Need:
	# name: string, name tof the need
	# schedule: Schedule, schedule of the need
	# subcategory: string, category of the organisations that can fulfill the need
	# strength: int, base amount of money needed to fulfill the need
	# strengthRandomRange: int, at each trigger of the need, up to this amount of money will randomly be added to the strength of the need trigger
	# isBuyAction: boolean, express if the TradeAction is a BuyAction or a PayAction
	def __init__(self, name, schedule, subcategory, strength, strengthRandomRange, isBuyAction):
		self.name = name
		self.schedule = schedule
		self.subcategory = subcategory
		self.strength = strength
		self.strengthRandomRange = strengthRandomRange
		self.isBuyAction = isBuyAction

