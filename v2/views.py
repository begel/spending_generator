from django.shortcuts import render
from .models import Profile
from common.models import *
import v2.profile2Needs as p2n
import datetime as d
import random as r

def web_page(request, profile):
    """ Just render the HTML and call the generator function """
    needs = p2n.generateAllNeeds(profile)
    main_people = profile.people.filter(adult=True)[0]
    tradeActions = generate_purchase(needs, main_people)
    tradeActions.sort(key=lambda x: x.startTime)
    context = {'tradeActions':tradeActions}
    return render(request, 'common/generate.html', context)

def generate_purchase(needs, person):
    ''' Convert needs of the family into purchases '''
    tradeActions = []
    for need in needs:
        if need.schedule.periodDay == 0:
            # Not a periodic need
                if r.randrange(100) >= need.schedule.triggerCancellationProbability:
                    purchase = TradeAction(is_buy_action = need.isBuyAction)
                    purchase.agent = person
                    purchase.object_rdfProp = Product(name=need.name, category=need.subcategory)
                    # No period, so no timedelta from the period
                    days = r.randrange(need.schedule.triggerTimeRandomRange+1)
                    purchase.startTime = need.schedule.firstDay + d.timedelta(days)
                    purchase.priceSpecification = PriceSpecification(price = need.strength + r.uniform(0,need.strengthRandomRange), priceCurrency = 'EUR')
                    purchase.seller = r.choice(Organization.objects.filter(subcategory=need.subcategory))
                    tradeActions.append(purchase)
        else:
            # That's a periodic need
            for i in range(int(365/need.schedule.periodDay)):
                if r.randrange(100) >= need.schedule.triggerCancellationProbability:
                    purchase = TradeAction(is_buy_action = need.isBuyAction)
                    purchase.agent = person
                    purchase.object_rdfProp = Product(name=need.name, category=need.subcategory)
                    days = i*need.schedule.periodDay + r.randrange(need.schedule.triggerTimeRandomRange+1)
                    purchase.startTime = need.schedule.firstDay + d.timedelta(days)
                    purchase.priceSpecification = PriceSpecification(price = need.strength + r.uniform(0,need.strengthRandomRange), priceCurrency = 'EUR')
                    purchase.seller = r.choice(Organization.objects.filter(subcategory=need.subcategory))
                    tradeActions.append(purchase)
    return tradeActions
