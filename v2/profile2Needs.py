from v2.NeedAndSchedule import *

# Register all the needs generators from the "needsGenerator" folder here
import v2.needsGenerators.weeklyFoodShopping as weeklyFoodShopping
import v2.needsGenerators.houseRelatedNeeds.rent as rent
import v2.needsGenerators.houseRelatedNeeds.water as water
import v2.needsGenerators.houseRelatedNeeds.electricity as electricity
import v2.needsGenerators.houseRelatedNeeds.localResidenceTax as localResidenceTax
import v2.needsGenerators.houseRelatedNeeds.heating as heating
import v2.needsGenerators.school as school
import v2.needsGenerators.carExpenses as carExpenses
import v2.needsGenerators.travels as travels
needGenerators = [
  weeklyFoodShopping,
  rent,
  water,
  electricity,
  localResidenceTax,
  heating,
  school,
  carExpenses,
  travels,
]

def generateAllNeeds(profile):
  needs = []
  for needGenerator in needGenerators:
    needs = needs + needGenerator.generateNeeds(profile)
  return needs
