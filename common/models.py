from __future__ import unicode_literals
from django.db import models

genders = (
    ('F', 'Female'),
    ('M', 'Male'),
)
scale = (
    ('1', 'Low'),
    ('2', 'Medium'),
    ('3', 'High'),
)
school = (
    ('No','no'),
    ('Elementary', 'Elementary school'),
    ('Middle', 'Middle school'),
    ('High', 'High school'),
    ('College', 'College'),
)

product = (
    ('Food', 'Food'),
    ('Clothing', 'Clothing'),
    ('Rent or Real Estate Loan', 'Rent or Real Estate Loan'),
    ('Bills', 'Bills'),
    ('Fuel', 'Fuel'),
    ('Saving', 'Saving'),
)

class GeoCoordinates(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return str(self.latitude) + ';' + str(self.longitude)

class Organization(models.Model):
    name = models.CharField(max_length=50)
    subcategory = models.CharField(max_length=50, default="Organization")
    location = models.ForeignKey(
        GeoCoordinates,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

class PriceSpecification(models.Model):
    price = models.IntegerField()
    EURO = 'EUR'
    USDOLLAR = 'USD'
    PRICE_CURRENCY_CHOICES = (
        (EURO, 'Euro'),
        (USDOLLAR, 'US Dollar'),
    )
    priceCurrency = models.CharField(
        max_length=3,
        choices=PRICE_CURRENCY_CHOICES,
        default=EURO,
    )

    def __str__(self):
        return str(self.price) + self.priceCurrency

class Person(models.Model):
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=genders, default='F')
    adult = models.BooleanField(default=True)
    health = models.CharField(max_length=1, choices=scale, default='1')
    school = models.CharField(max_length=10, choices=school, default='no')
    work_distance = models.CharField(max_length=1, choices=scale, default='1')

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(
        max_length=25,
        choices=product,
        default='Food',
    )

    def __str__(self):
        return self.name

class TradeAction(models.Model):
    is_buy_action = models.BooleanField(default=True)
    seller = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
    )
    priceSpecification = models.ForeignKey(
        PriceSpecification,
        on_delete=models.CASCADE,
    )
    agent = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
    )
    object_rdfProp = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
    )
    startTime = models.DateTimeField

    def __str__(self):
        return self.startTime
