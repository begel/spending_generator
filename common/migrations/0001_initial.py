# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 22:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GeoCoordinates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=500)),
                ('subcategory', models.CharField(default='Organization', max_length=50)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.GeoCoordinates')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[('F', 'Female'), ('M', 'Male')], default='F', max_length=1)),
                ('adult', models.BooleanField(default=True)),
                ('health', models.CharField(choices=[('1', 'Low'), ('2', 'Medium'), ('3', 'High')], default='1', max_length=1)),
                ('school', models.CharField(choices=[('No', 'no'), ('Elementary', 'Elementary school'), ('Middle', 'Middle school'), ('High', 'High school'), ('College', 'College')], default='no', max_length=10)),
                ('work_distance', models.CharField(choices=[('1', 'Low'), ('2', 'Medium'), ('3', 'High')], default='1', max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='PriceSpecification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.IntegerField()),
                ('priceCurrency', models.CharField(choices=[('EUR', 'Euro'), ('USD', 'US Dollar')], default='EUR', max_length=3)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('category', models.CharField(choices=[('Food', 'Food'), ('Clothing', 'Clothing'), ('Rent or Real Estate Loan', 'Rent or Real Estate Loan'), ('Bills', 'Bills'), ('Fuel', 'Fuel'), ('Saving', 'Saving')], default='Food', max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='TradeAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_buy_action', models.BooleanField(default=True)),
                ('agent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.Person')),
                ('object_rdfProp', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.Product')),
                ('priceSpecification', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.PriceSpecification')),
                ('seller', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.Organization')),
            ],
        ),
    ]
