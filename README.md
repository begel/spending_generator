# Projet pour le cours Web Data Managment

## Installation
Pour mettre en place le virtualenv, la base de données et peupler la base de données, exécuter le fichier init.sh.

Avant chaque utilisation du projet, activer le virtualenv `source .env/bin/activate` à la racine.
Pour quitter l'environnement de production, `deactivate`. 

Pour lancer le serveur en local : `./manage.py runserver`

## But
Générer une liste de dépenses annuelles à partir d'un profil de foyer.

## Utilisation
L'initialisation de la base de données crée suffisamment d'Organization et de GeoCoordinates pour lancer les générations. Mais vous pouvez en rajouter dans l'interface web.

On crée un Profile dans la version du générateur que l'on souhaite tester. Pour la v2, il faut renseigner les Person du foyer que l'on veut utiliser (au moins 1 adulte).

On coche ensuite le Profile et le menu déroulant propose l'option "Generate new spendings".
