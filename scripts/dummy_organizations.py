import csv
import datetime as d
from common.models import GeoCoordinates, Organization

def run():
    g = GeoCoordinates()
    g.longitude = 2.2935088
    g.latitude = 48.8583513
    g.save()
    with open('from_insee.csv') as f:
        r = csv.reader(f, delimiter=',')
        for p in r:
            o = Organization()
            o.location = g
            o.subcategory = p[0]
            o.name = p[0]
            o.save()
