import csv
from common.models import GeoCoordinates, Organization

def run():
    with open('organizations_palaiseau.csv') as f:
        r = csv.DictReader(f, delimiter=',')
        for p in r:
            g = GeoCoordinates(latitude=p['Latitude'], longitude=p['Longitude'])
            g.save()
            o = Organization(name=p['Name'], subcategory=p['Subcategory'], location=g)
            o.save()
