#!/bin/bash
echo '### Initialisation du projet'
echo '### Mise en place du virtualenv'
virtualenv .env --python=python3
. .env/bin/activate
pip install django
pip install django-extensions

echo '### Initialisation de la base de données'
sed -i -e ' 16,19 s/^/# /g' common/admin.py
./manage.py migrate
sed -i -e ' 16,19 s/^# //g' common/admin.py

echo '### Remplissage de la base de données'
./manage.py runscript dummy_organizations
./manage.py runscript import_organizations
