# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 23:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('common', '0002_remove_organization_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Default', max_length=50)),
                ('salary', models.IntegerField()),
                ('people', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='v1_profile', to='common.Person')),
            ],
        ),
    ]
