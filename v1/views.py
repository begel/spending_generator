from django.shortcuts import render
from .models import Profile
from common.models import *
import datetime as d
import csv

dataCSV = 'from_insee.csv'

def web_page(request, profile):
    """ Just render the HTML and call the generate function """
    d = distribute(profile.salary, distribution())
    main_people = Person(name=profile.name)
    tradeActions = generate(profile, d, main_people)
    tradeActions.sort(key=lambda x: x.startTime)
    context = {'salary' : profile.salary, 'tradeActions' : tradeActions}
    return render(request, 'common/generate.html', context)

def distribution():
    """ From a CSV file, read the first line to get the distribution of salary.
    The result is a list of string of integer. """
    with open(dataCSV) as f:
        reader = csv.reader(f, delimiter=',')
        distribution = next(reader)[2:]
        return distribution

def distribute(salary, distribution):
    """ Associate to the income the corresponding decile. The distribution is a list of string of integer. The result is the key to use of the DictReader. """
    for d in distribution:
        if salary <= int(d):
            return d
    distribution.reverse()
    return distribution[0]

def generate(profile, d, person):
    """ Split the money in the different categories. The input are the Profile
    with the salary and the corresponding category. """
    with open(dataCSV) as f:
        reader = csv.DictReader(f)
        s = profile.salary
        tradeActions = []
        tradeActions.extend(savings(s*0.1, person))
        for r in reader:
            org = Organization.objects.filter(subcategory=r['Subcategory'])[0]
            tradeActions.extend(purchase(s*float(r[d])/100, person, org, r['TradeAction'], weeks=4))
        return tradeActions 

def extract(kwargs, field, default):
    if field in kwargs:
        return kwargs[field]
    else:
        return default 
   
def purchase(price, person, organization, trade, **kwargs):
    """ The general scheme to generate regular purchases for a category """
    tradeActions = []
    firstDay = extract(kwargs, 'firstDay', d.datetime(2017,1,3,18,5))
    weeks = extract(kwargs, 'weeks', 0)
    name = extract(kwargs, 'name', '')
    category = extract(kwargs, 'category', organization.subcategory)
    if weeks ==0:
        raise Exception('You must define the frequency')
    timesAYear = extract(kwargs, 'timesAYear', int(52/(weeks)))
    priceOnce = price / timesAYear
    is_buy_action = trade == 'BuyAction'
    for i in range(timesAYear):
        purchase = TradeAction(is_buy_action = is_buy_action)
        purchase.startTime = firstDay + i * d.timedelta(days=14) * weeks
        purchase.agent = person
        purchase.priceSpecification = PriceSpecification(price = priceOnce, priceCurrency = 'EUR')
        purchase.object_rdfProp = Product(name=name, category=category)
        purchase.seller = organization
        tradeActions.append(purchase)
    return tradeActions
        
        
def savings(price, person):
    """ Let's say that we save the same amount each month """
    tradeActions = []
    day = d.datetime(2017,1,27,12)
    priceAMonth = price / 12
    for i in range(12):
        purchase = TradeAction(is_buy_action=False)
        purchase.startTime = day.replace(month = i+1)
        purchase.agent = person
        purchase.priceSpecification = PriceSpecification(price = priceAMonth, priceCurrency = 'EUR')
        purchase.object_rdfProp = Product(name="Epargne", category='Saving')
        purchase.seller = Organization.objects.filter(subcategory="BankOrCreditUnion")[0]
        tradeActions.append(purchase)
    return tradeActions
